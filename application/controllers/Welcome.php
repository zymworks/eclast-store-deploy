<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();
		$this->load->database();
		$this->load->helper('url');
	}
	public function index()
	{
 
		$this->load->view('smart_bazzar/products');

	}

	public function testDB()
	{
		$query = $this->db->query('SELECT * FROM master_product');
		//echo $query;
		echo 'Total Results: ' . $query->num_rows();
		//$this->load->view('welcome_message');

	}
	public function tampilkanSemua()
	{
		$this->db->select('*');
        $query = $this->db->get('master_product');

        echo json_encode($query->result(),JSON_PRETTY_PRINT);
    }
	
}
