-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 11 Nov 2017 pada 13.41
-- Versi Server: 10.1.25-MariaDB
-- PHP Version: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eclast_store`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_category`
--

CREATE TABLE `master_category` (
  `id` int(15) NOT NULL,
  `cat_name` varchar(30) NOT NULL,
  `cat_slug` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_category`
--

INSERT INTO `master_category` (`id`, `cat_name`, `cat_slug`) VALUES
(1, 'outdoor stuff', 'outdoor-stuff');

-- --------------------------------------------------------

--
-- Struktur dari tabel `master_product`
--

CREATE TABLE `master_product` (
  `id` mediumint(12) NOT NULL,
  `id_category` int(12) NOT NULL,
  `eclast_code` varchar(30) DEFAULT NULL,
  `supplier_code` varchar(30) DEFAULT NULL,
  `name` varchar(200) DEFAULT NULL,
  `supplier_price` float DEFAULT '0',
  `eclast_price` float NOT NULL DEFAULT '0',
  `description` text,
  `main_image` varchar(50) DEFAULT NULL,
  `date_created` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data untuk tabel `master_product`
--

INSERT INTO `master_product` (`id`, `id_category`, `eclast_code`, `supplier_code`, `name`, `supplier_price`, `eclast_price`, `description`, `main_image`, `date_created`) VALUES
(265, 1, 'ECL-2-1', '199084719', 'Sepatu Original Nike Air Force 1 - Red/White', 1379000, 1386000, NULL, 'ecl-sp-199084719', '2017-11-10 20:56:13'),
(266, 1, 'ECL-2-2', '174269980', 'Original Sepatu Nike Air Force 1 Men - Hitam', 1379000, 1386000, NULL, 'ecl-sp-174269980', '2017-11-10 20:56:14'),
(267, 1, 'ECL-2-3', '227277498', 'Sepatu Original Nike Flex Contact - Gym Blue', 999000, 1006000, NULL, 'ecl-sp-227277498', '2017-11-10 20:56:14'),
(268, 1, 'ECL-2-4', '227276330', 'Sepatu Original Nike Zoom Shift - Wolf Grey White', 1499000, 1506000, NULL, 'ecl-sp-227276330', '2017-11-10 20:56:15'),
(269, 1, 'ECL-2-5', '227231678', 'Sepatu Running Original Nike Flex Experience RN 6 - Black/Hot Punch', 899000, 906000, NULL, 'ecl-sp-227231678', '2017-11-10 20:56:16'),
(270, 1, 'ECL-2-6', '64567073', 'Original Nike Air Relentless 6 MSL - Hitam/Putih', 949000, 956000, NULL, 'ecl-sp-64567073', '2017-11-10 20:56:17'),
(271, 1, 'ECL-2-7', '227222796', 'Sepatu Original Under Armour UA Micro G Speed Swift 2 M - Black/White', 1299000, 1306000, NULL, 'ecl-sp-227222796', '2017-11-10 20:56:17'),
(272, 1, 'ECL-2-8', '227222141', 'Sepatu Original Under Armour UA Micro G Speed Swift 2 W - Black/White', 1299000, 1306000, NULL, 'ecl-sp-227222141', '2017-11-10 20:56:18'),
(273, 1, 'ECL-2-9', '227082056', 'Sepatu Original Under Armour UA Micro G Speed Swift 2 - Black/Red', 1599000, 1606000, NULL, 'ecl-sp-227082056', '2017-11-10 20:56:19'),
(274, 1, 'ECL-2-10', '227076591', 'Sepatu Original Under Armour UA Micro G Fuel - London Orange', 1599000, 1606000, NULL, 'ecl-sp-227076591', '2017-11-10 20:56:19'),
(275, 1, 'ECL-2-11', '209193728', 'Sepatu Original Nike Sock Dart - Black/White', 1979000, 1986000, NULL, 'ecl-sp-209193728', '2017-11-10 20:56:20'),
(276, 1, 'ECL-2-12', '212566197', 'Sepatu Running Original Adidas Pureboost M - Grey/White', 2199000, 2206000, NULL, 'ecl-sp-212566197', '2017-11-10 20:56:21'),
(277, 1, 'ECL-2-13', '137000100', 'Sepatu Adidas Alpha Bounce RC Men - Hitam', 999000, 1006000, NULL, 'ecl-sp-137000100', '2017-11-10 20:56:21'),
(278, 1, 'ECL-2-14', '222634523', 'Sepatu Original Nike Jordan Ultra Fly 2 - Black/White', 1899000, 1906000, NULL, 'ecl-sp-222634523', '2017-11-10 20:56:22'),
(279, 1, 'ECL-2-15', '150778566', 'Original Sepatu Nike Kaishi 2.0 - Midnight Navy', 999000, 1006000, NULL, 'ecl-sp-150778566', '2017-11-10 20:56:23'),
(280, 1, 'ECL-2-16', '221473661', 'Sepatu Running Original Nike Lunarglide 8 - Multicolor', 1399000, 1406000, NULL, 'ecl-sp-221473661', '2017-11-10 20:56:24'),
(281, 1, 'ECL-2-17', '54746299', 'Original Sepatu Running Legas Neptune La M Cloud Burst', 409000, 416000, NULL, 'ecl-sp-54746299', '2017-11-10 20:56:24'),
(282, 1, 'ECL-2-18', '73231838', 'Original Sepatu League Neptune La M Cloud Burst', 409000, 416000, NULL, 'ecl-sp-73231838', '2017-11-10 20:56:25'),
(283, 1, 'ECL-2-19', '145426836', 'Sepatu Running Original Nike Free Training 6 - Putih/Merah', 1499000, 1506000, NULL, 'ecl-sp-145426836', '2017-11-10 20:56:26'),
(284, 1, 'ECL-2-20', '137879011', 'Sepatu Original Nike Metcon 3 Women - Hitam/Peach', 1979000, 1986000, NULL, 'ecl-sp-137879011', '2017-11-10 20:56:27'),
(285, 1, 'ECL-2-21', '187716200', 'Original Sepatu Nike Air Max 90 Ultra 2.0 Essential - White Platinum', 1799000, 1806000, NULL, 'ecl-sp-187716200', '2017-11-10 20:56:27'),
(286, 1, 'ECL-2-22', '181979944', 'Sepatu Training Original Adidas Adipure Flex - Blue/White', 1399000, 1406000, NULL, 'ecl-sp-181979944', '2017-11-10 20:56:28'),
(287, 1, 'ECL-2-23', '193197536', 'Sepatu Nike Air Max VIsion Original - White/Navy', 1379000, 1386000, NULL, 'ecl-sp-193197536', '2017-11-10 20:56:28'),
(288, 1, 'ECL-2-24', '199023951', 'Original Sepatu Nike Air Max 90 Ultra 2.0 Essential - Wolf Grey', 1799000, 1806000, NULL, 'ecl-sp-199023951', '2017-11-10 20:56:29'),
(289, 1, 'ECL-2-25', '193350882', 'Sepatu Running Original Asics Gel Nimbus 19 - Black/Grey', 2599000, 2606000, NULL, 'ecl-sp-193350882', '2017-11-10 20:56:30'),
(290, 1, 'ECL-2-26', '221481315', 'Sepatu Original Under Armour UA SpeedForm Slingride - Ultra Blue', 2199000, 2206000, NULL, 'ecl-sp-221481315', '2017-11-10 20:56:30'),
(291, 1, 'ECL-2-27', '225302394', 'Sepatu Original Nike Lunar Apparent - Hot Siren White', 1279000, 1286000, NULL, 'ecl-sp-225302394', '2017-11-10 20:56:31'),
(292, 1, 'ECL-2-28', '225300725', 'Sepatu Original Nike Air Max Motion Racer - Black/White', 1499000, 1506000, NULL, 'ecl-sp-225300725', '2017-11-10 20:56:32'),
(293, 1, 'ECL-2-29', '225300008', 'Sepatu Original Nike Air Max Motion Racer Men - Dust Solar Red', 1499000, 1506000, NULL, 'ecl-sp-225300008', '2017-11-10 20:56:33'),
(294, 1, 'ECL-2-30', '222591113', 'Sepatu Running Original Nike Air Zoom Pegasus 34 - Black/White ()', 1799000, 1806000, NULL, 'ecl-sp-222591113', '2017-11-10 20:56:33'),
(295, 1, 'ECL-2-31', '225271313', 'Sepatu Original DC Haven Tx SE - Denim', 749000, 756000, NULL, 'ecl-sp-225271313', '2017-11-10 20:56:34'),
(296, 1, 'ECL-2-32', '151048920', 'Sepatu Running Original Nike Air Zoom Pegasus 33 - Black', 1799000, 1806000, NULL, 'ecl-sp-151048920', '2017-11-10 20:56:35'),
(297, 1, 'ECL-2-33', '142081521', 'Sepatu Running Original Nike Air Relentless 6 MSL - Merah', 949000, 956000, NULL, 'ecl-sp-142081521', '2017-11-10 20:56:35'),
(298, 1, 'ECL-2-34', '170194427', 'Sepatu Adidas Alphabounce Lux - White/Grey', 1599000, 1606000, NULL, 'ecl-sp-170194427', '2017-11-10 20:56:36'),
(299, 1, 'ECL-2-35', '201983353', 'Sepatu Original Adidas Alphabounce Lux -  Grey Three', 1699000, 1706000, NULL, 'ecl-sp-201983353', '2017-11-10 20:56:37'),
(300, 1, 'ECL-2-36', '220142204', 'Sepatu Running Original Adidas Vengeful - Mystery Blue []', 1599000, 1606000, NULL, 'ecl-sp-220142204', '2017-11-10 20:56:37'),
(301, 1, 'ECL-2-37', '220143513', 'Sepatu Running Original Sepatu Adidas Vengeful - Navy/White', 1599000, 1606000, NULL, 'ecl-sp-220143513', '2017-11-10 20:56:38'),
(302, 1, 'ECL-2-38', '225074366', 'Sepatu Original Nike Zoom Train Action - Black/White', 1499000, 1506000, NULL, 'ecl-sp-225074366', '2017-11-10 20:56:38'),
(303, 1, 'ECL-2-39', '213898979', 'Sepatu Nike Air Huarache Original - Black Gym Red', 1649000, 1656000, NULL, 'ecl-sp-213898979', '2017-11-10 20:56:39'),
(304, 1, 'ECL-2-40', '210399677', 'Sepatu Original Nike Air Presto Fly  - Midnight Navy', 1499000, 1506000, NULL, 'ecl-sp-210399677', '2017-11-10 20:56:40'),
(305, 1, 'ECL-2-41', '182997570', 'Sepatu Basket Original Nike Jordan Super Fly 5 PO - Black', 2099000, 2106000, NULL, 'ecl-sp-182997570', '2017-11-10 20:56:40'),
(306, 1, 'ECL-2-42', '216695468', 'Sepatu Running Original Nike Lunarepic Low Flyknit 2 - Triple Black', 2099000, 2106000, NULL, 'ecl-sp-216695468', '2017-11-10 20:56:41'),
(307, 1, 'ECL-2-43', '214155946', 'Sepatu Formal Kickers Leatherette boots Original - Brown', 2299000, 2306000, NULL, 'ecl-sp-214155946', '2017-11-10 20:56:42'),
(308, 1, 'ECL-2-44', '200458221', 'Sepatu Running Original Nike Free Rn - Hyper Orange Black', 1649000, 1656000, NULL, 'ecl-sp-200458221', '2017-11-10 20:56:42'),
(309, 1, 'ECL-2-45', '184995265', 'Sepatu Basket Original Nike Jordan Super Fly 5 PO - Red', 2099000, 2106000, NULL, 'ecl-sp-184995265', '2017-11-10 20:56:43'),
(310, 1, 'ECL-2-46', '179433285', 'Sepatu Running Original Nike Free RN Commuter 2017 - White Platinum', 1649000, 1656000, NULL, 'ecl-sp-179433285', '2017-11-10 20:56:44'),
(311, 1, 'ECL-2-47', '209198714', 'Sepatu Original Nike Free RN Commuter 2017 M - Black', 1649000, 1656000, NULL, 'ecl-sp-209198714', '2017-11-10 20:56:45'),
(312, 1, 'ECL-2-48', '159103732', 'Sepatu Adidas Originals Stan Smith CF - White/Green', 1699000, 1706000, NULL, 'ecl-sp-159103732', '2017-11-10 20:56:46'),
(313, 1, 'ECL-2-49', '193062275', 'Sepatu Original Adidas Caprock - Black', 1599000, 1606000, NULL, 'ecl-sp-193062275', '2017-11-10 20:56:46'),
(314, 1, 'ECL-2-50', '199005712', 'Sepatu Adidas Originals Swift Run Sneakers - Core Black', 1499000, 1506000, NULL, 'ecl-sp-199005712', '2017-11-10 20:56:47'),
(315, 1, 'ECL-2-51', '214166835', 'Sepatu Running Trail Original Adidas Terrex CMTK GTX - Black', 1799000, 1806000, NULL, 'ecl-sp-214166835', '2017-11-10 20:56:48'),
(316, 1, 'ECL-2-52', '193056701', 'Sepatu Original Adidas Crazytrain Elite - Silver/White', 2299000, 2306000, NULL, 'ecl-sp-193056701', '2017-11-10 20:56:48'),
(317, 1, 'ECL-2-53', '191691491', 'Sepatu Original Adidas Crazytrain Elite - Black Red Scarlet', 2299000, 2306000, NULL, 'ecl-sp-191691491', '2017-11-10 20:56:49'),
(318, 1, 'ECL-2-54', '211663248', 'Sepatu Adidas Originals Eqt Support Adv - White Grey One ', 2299000, 2306000, NULL, 'ecl-sp-211663248', '2017-11-10 20:56:50'),
(319, 1, 'ECL-2-55', '224684776', 'Sepatu Original Onitsuka Tiger Ultimate 81 - Black Latte', 1299000, 1306000, NULL, 'ecl-sp-224684776', '2017-11-10 20:56:50'),
(320, 1, 'ECL-2-56', '224684438', 'Sepatu Original Onitsuka Tiger Mexico Delegation - Burgandy', 1699000, 1706000, NULL, 'ecl-sp-224684438', '2017-11-10 20:56:51'),
(321, 1, 'ECL-2-57', '224683559', 'Sepatu Original Onitsuka Tiger Monte Z - Feather Grey', 1699000, 1706000, NULL, 'ecl-sp-224683559', '2017-11-10 20:56:52'),
(322, 1, 'ECL-2-58', '213020413', 'Sepatu Original Adidas Energy Boost - Black/White', 2299000, 2306000, NULL, 'ecl-sp-213020413', '2017-11-10 20:56:53'),
(323, 1, 'ECL-2-59', '213021077', 'Sepatu Original Adidas Energy Boost - Grey/White', 2299000, 2306000, NULL, 'ecl-sp-213021077', '2017-11-10 20:56:54'),
(324, 1, 'ECL-2-60', '198948261', 'Sepatu Running Original Adidas Energy Boost - Blue/Navy', 2299000, 2306000, NULL, 'ecl-sp-198948261', '2017-11-10 20:56:55'),
(325, 1, 'ECL-2-61', '187701176', 'Sepatu Running Original Adidas Pureboost dpr - Grey/Blue', 2499000, 2506000, NULL, 'ecl-sp-187701176', '2017-11-10 20:56:56'),
(326, 1, 'ECL-2-62', '218686824', 'Sepatu Adidas Originals Tubular Doom Primeknit - Black/Grey', 2499000, 2506000, NULL, 'ecl-sp-218686824', '2017-11-10 20:56:57'),
(327, 1, 'ECL-2-63', '220984667', 'Sepatu Running Original Adidas Ultraboost ST - Blue/White', 2899000, 2906000, NULL, 'ecl-sp-220984667', '2017-11-10 20:56:58'),
(328, 1, 'ECL-2-64', '224674066', 'Sepatu Original Onitsuka Tiger Mexico Delegation - Mid/Grey', 1699000, 1706000, NULL, 'ecl-sp-224674066', '2017-11-10 20:56:59'),
(329, 1, 'ECL-2-65', '224673804', 'Sepatu Original Onitsuka Tiger Serrano - Skyway/Blue', 1299000, 1306000, NULL, 'ecl-sp-224673804', '2017-11-10 20:56:59'),
(330, 1, 'ECL-2-66', '224673530', 'Sepatu Original Onitsuka Mexico 66 - White/Blue', 1599000, 1606000, NULL, 'ecl-sp-224673530', '2017-11-10 20:57:00'),
(331, 1, 'ECL-2-67', '222637102', 'Sepatu Running Original Nike Air Zoom Winflo 4 - Deep Royal Blue', 1379000, 1386000, NULL, 'ecl-sp-222637102', '2017-11-10 20:57:01'),
(332, 1, 'ECL-2-68', '187698126', 'Sepatu Adidas Originals Tubular Viral 2 - Black/White', 1599000, 1606000, NULL, 'ecl-sp-187698126', '2017-11-10 20:57:01'),
(333, 1, 'ECL-2-69', '209492779', 'Sepatu Adidas Originals Tubular Doom Sock Primeknit - Pastel/White', 1999000, 2006000, NULL, 'ecl-sp-209492779', '2017-11-10 20:57:02'),
(334, 1, 'ECL-2-70', '150462523', 'Original Sepatu Nike Kaishi 2.0 Printed - Multi Colour', 999000, 1006000, NULL, 'ecl-sp-150462523', '2017-11-10 20:57:03'),
(335, 1, 'ECL-2-71', '199031370', 'Sepatu Original Nike Dunk Low Sneakers - White', 1379000, 1386000, NULL, 'ecl-sp-199031370', '2017-11-10 20:57:04'),
(336, 1, 'ECL-2-72', '224503664', 'Sepatu Running Original Adidas Duramo Lite - Mystery Blue', 749000, 756000, NULL, 'ecl-sp-224503664', '2017-11-10 20:57:05'),
(337, 1, 'ECL-2-73', '224501777', 'Sepatu Original Onitsuka Mexico 66 - Dark Grey', 1599000, 1606000, NULL, 'ecl-sp-224501777', '2017-11-10 20:57:06'),
(338, 1, 'ECL-2-74', '224500780', 'Sepatu Adidas Originals Adi-Ease - Black/White', 1099000, 1106000, NULL, 'ecl-sp-224500780', '2017-11-10 20:57:07'),
(339, 1, 'ECL-2-75', '176756505', 'Sepatu Adidas Originals Adi Ease - White/Blue', 1099000, 1106000, NULL, 'ecl-sp-176756505', '2017-11-10 20:57:08'),
(340, 1, 'ECL-2-76', '224497461', 'Sepatu Adidas Originals Adi-Ease - Grey/White', 1099000, 1106000, NULL, 'ecl-sp-224497461', '2017-11-10 20:57:11'),
(341, 1, 'ECL-2-77', '224398111', 'Sepatu Original Onitsuka Edr 78 - Peacoat', 1299000, 1306000, NULL, 'ecl-sp-224398111', '2017-11-10 20:57:12'),
(342, 1, 'ECL-2-78', '224396750', 'Sepatu Original Onitsuka Tiger Monte Creace - Black White', 1699000, 1706000, NULL, 'ecl-sp-224396750', '2017-11-10 20:57:13'),
(343, 1, 'ECL-2-79', '224396111', 'Sepatu Original Onitsuka Tiger Monte Z - Black White', 1699000, 1706000, NULL, 'ecl-sp-224396111', '2017-11-10 20:57:13'),
(344, 1, 'ECL-2-80', '224395033', 'Sepatu Original Onitsuka Tiger Monte Creace - Grey Black White', 1699000, 1706000, NULL, 'ecl-sp-224395033', '2017-11-10 20:57:14'),
(345, 1, 'ECL-2-81', '208929057', 'Sepatu Original Nike Air Presto Essential - Black/White', 1799000, 1806000, NULL, 'ecl-sp-208929057', '2017-11-10 20:57:15'),
(346, 1, 'ECL-2-82', '199026534', 'Original Sepatu Nike Air Max 1 Ultra 2.0 Essential - White Platinum', 1799000, 1806000, NULL, 'ecl-sp-199026534', '2017-11-10 20:57:15'),
(347, 1, 'ECL-2-83', '71568487', 'Original Sepatu Nike W Air Max BW Ultra Mid Navy', 1399000, 1406000, NULL, 'ecl-sp-71568487', '2017-11-10 20:57:16'),
(348, 1, 'ECL-2-84', '186666022', 'Sepatu Original Nike Flex Tr 7 - Wolf Grey', 999000, 1006000, NULL, 'ecl-sp-186666022', '2017-11-10 20:57:16'),
(349, 1, 'ECL-2-85', '202116132', 'Sepatu Original Nike Air Max 90 Ultra 2.0 Essential - Grey Red', 1799000, 1806000, NULL, 'ecl-sp-202116132', '2017-11-10 20:57:18'),
(350, 1, 'ECL-2-86', '190259514', 'Sepatu Original Nike Air Max \'90 Essential - Black/Gym Red', 1649000, 1656000, NULL, 'ecl-sp-190259514', '2017-11-10 20:57:18'),
(351, 1, 'ECL-2-87', '207915445', 'Sepatu Nike Air Max Motion 16 LW Original - Black/White', 1279000, 1286000, NULL, 'ecl-sp-207915445', '2017-11-10 20:57:20'),
(352, 1, 'ECL-2-88', '224218531', 'Sepatu Adidas Originals Stan Smith - Tactile Rose', 1499000, 1506000, NULL, 'ecl-sp-224218531', '2017-11-10 20:57:22'),
(353, 1, 'ECL-2-89', '214157917', 'Sepatu Formal Kickers Casual Leather 2505 Original - Black', 1949000, 1956000, NULL, 'ecl-sp-214157917', '2017-11-10 20:57:22'),
(354, 1, 'ECL-2-90', '219114081', 'Sepatu Original Adidas Alphabounce Zip - Black', 1799000, 1806000, NULL, 'ecl-sp-219114081', '2017-11-10 20:57:23'),
(355, 1, 'ECL-2-91', '223974070', 'Original Sepatu Nike Juvenate - Glacier Grey', 1279000, 1286000, NULL, 'ecl-sp-223974070', '2017-11-10 20:57:23'),
(356, 1, 'ECL-2-92', '198555851', 'Original Sepatu Nike Tanjun Racer - Hitam', 949000, 956000, NULL, 'ecl-sp-198555851', '2017-11-10 20:57:25'),
(357, 1, 'ECL-2-93', '223823958', 'Sepatu Original Asics Tiger Gel-Kayano Trainer Knit - Agave Green []', 2299000, 2306000, NULL, 'ecl-sp-223823958', '2017-11-10 20:57:26'),
(358, 1, 'ECL-2-94', '200529433', 'Sepatu Original Asics Tiger Gel-Lyte III - Bay/White', 1499000, 1506000, NULL, 'ecl-sp-200529433', '2017-11-10 20:57:26'),
(359, 1, 'ECL-2-95', '223815706', 'Sepatu Original Asics Tiger Gel-Kayano Trainer Knit - Bleached/Apricot', 2299000, 2306000, NULL, 'ecl-sp-223815706', '2017-11-10 20:57:27'),
(360, 1, 'ECL-2-96', '223814696', 'Sepatu Original Asics Tiger Gel-Kayano Trainer Knit - Viridian Green', 2299000, 2306000, NULL, 'ecl-sp-223814696', '2017-11-10 20:57:27'),
(361, 1, 'ECL-2-97', '223812223', 'Sepatu Original Asics Tiger Gel-Lyte III - Bleached/Apricot', 1499000, 1506000, NULL, 'ecl-sp-223812223', '2017-11-10 20:57:28'),
(362, 1, 'ECL-2-98', '223809686', 'Sepatu Original Asics Tiger Gel-Kayano Trainer Knit - Imperial Blue', 2299000, 2306000, NULL, 'ecl-sp-223809686', '2017-11-10 20:57:29'),
(363, 1, 'ECL-2-99', '223792617', 'Sepatu Original Nike Court Borough Low Sneakers - Black/White', 899000, 906000, NULL, 'ecl-sp-223792617', '2017-11-10 20:57:29'),
(364, 1, 'ECL-2-100', '223760968', 'Sepatu Original Reebok Zoku Runner G - Smoky Indigo', 1299000, 1306000, NULL, 'ecl-sp-223760968', '2017-11-10 20:57:30'),
(365, 1, 'ECL-2-101', '223759271', 'Sepatu Original Reebok Cardio Motion - Red Rose', 899000, 906000, NULL, 'ecl-sp-223759271', '2017-11-10 20:57:31'),
(366, 1, 'ECL-2-102', '223756122', 'Sepatu Running Original Nike Air Zoom Winflo 4 - Black/White', 1379000, 1386000, NULL, 'ecl-sp-223756122', '2017-11-10 20:57:31'),
(367, 1, 'ECL-2-103', '223754287', 'Sepatu Running Original Nike Free RN 2017 - Black/Green', 1499000, 1506000, NULL, 'ecl-sp-223754287', '2017-11-10 20:57:32'),
(368, 1, 'ECL-2-104', '223752735', 'Sepatu Original Nike LunarSolo - Black/Cool Grey', 1499000, 1506000, NULL, 'ecl-sp-223752735', '2017-11-10 20:57:32'),
(369, 1, 'ECL-2-105', '223751045', 'Sepatu Original Nike Sock Dart SE Premium - Black/Light Bone', 2099000, 2106000, NULL, 'ecl-sp-223751045', '2017-11-10 20:57:33'),
(370, 1, 'ECL-2-106', '223608884', 'Sepatu Original Nike Blazer Low LE - Triple White', 1129000, 1136000, NULL, 'ecl-sp-223608884', '2017-11-10 20:57:34'),
(371, 1, 'ECL-2-107', '223607616', 'Sepatu Original Nike Blazer Low LE - Metallic Field', 1129000, 1136000, NULL, 'ecl-sp-223607616', '2017-11-10 20:57:34'),
(372, 1, 'ECL-2-108', '223603020', 'Sepatu Original Nike LunarSolo - Dark Sky Blue', 1499000, 1506000, NULL, 'ecl-sp-223603020', '2017-11-10 20:57:35'),
(373, 1, 'ECL-2-109', '223601396', 'Sepatu Original Nike Air Max Prime - Obsidian/Wolf Grey', 1649000, 1656000, NULL, 'ecl-sp-223601396', '2017-11-10 20:57:36'),
(374, 1, 'ECL-2-110', '223599363', 'Sepatu Original Nike Run Swift - Cool Grey', 949000, 956000, NULL, 'ecl-sp-223599363', '2017-11-10 20:57:37'),
(375, 1, 'ECL-2-111', '223598648', 'Sepatu Original Nike Dualtone Racer - Port Wine', 1379000, 1386000, NULL, 'ecl-sp-223598648', '2017-11-10 20:57:37'),
(376, 1, 'ECL-2-112', '223574831', 'Sepatu Original Reebok Zoku Runner W&W - Shell Pink', 1299000, 1306000, NULL, 'ecl-sp-223574831', '2017-11-10 20:57:38'),
(377, 1, 'ECL-2-113', '223561105', 'Sepatu Original Reebok Cl Leather Ebk - Smoky Indigo', 1099000, 1106000, NULL, 'ecl-sp-223561105', '2017-11-10 20:57:38'),
(378, 1, 'ECL-2-114', '223557399', 'Sepatu Pantofel Formal Kickers Leather 2301 Original - Light Brown', 1749000, 1756000, NULL, 'ecl-sp-223557399', '2017-11-10 20:57:39'),
(379, 1, 'ECL-2-115', '223556312', 'Sepatu Pantofel Formal Kickers Leather 2112 Original - Black', 1829000, 1836000, NULL, 'ecl-sp-223556312', '2017-11-10 20:57:40'),
(380, 1, 'ECL-2-116', '223555502', 'Sepatu Pantofel Formal Kickers Leather 2111 Original - Dark Brown', 1829000, 1836000, NULL, 'ecl-sp-223555502', '2017-11-10 20:57:41'),
(381, 1, 'ECL-2-117', '223552969', 'Sepatu Pantofel Formal Kickers Leather 2111 Original - Black', 1829000, 1836000, NULL, 'ecl-sp-223552969', '2017-11-10 20:57:41'),
(382, 1, 'ECL-2-118', '223551426', 'Sepatu Pantofel Formal Kickers Leather 2306 Original - Black', 1769000, 1776000, NULL, 'ecl-sp-223551426', '2017-11-10 20:57:42'),
(383, 1, 'ECL-2-119', '223550690', 'Sepatu Pantofel Formal Kickers Leather 2303 Original - Black', 1769000, 1776000, NULL, 'ecl-sp-223550690', '2017-11-10 20:57:43'),
(384, 1, 'ECL-2-120', '223549657', 'Sepatu Pantofel Formal Kickers Leather 2117 Original - Black', 1749000, 1756000, NULL, 'ecl-sp-223549657', '2017-11-10 20:57:43'),
(385, 1, 'ECL-2-121', '223548570', 'Sepatu Pantofel Formal Kickers Leather 2116 Original - Black', 1699000, 1706000, NULL, 'ecl-sp-223548570', '2017-11-10 20:57:44'),
(386, 1, 'ECL-2-122', '223413130', 'Sepatu Running Original Nike Free RN 2017 - Glacier Blue', 1499000, 1506000, NULL, 'ecl-sp-223413130', '2017-11-10 20:57:45'),
(387, 1, 'ECL-2-123', '223411136', 'Sepatu Original Nike CK Racer - Dark Raisin', 1129000, 1136000, NULL, 'ecl-sp-223411136', '2017-11-10 20:57:45'),
(388, 1, 'ECL-2-124', '223396673', 'Sepatu Pantofel Formal Kickers Leather 2112 Original - Dark Brown', 1699000, 1706000, NULL, 'ecl-sp-223396673', '2017-11-10 20:57:46'),
(389, 1, 'ECL-2-125', '223396146', 'Sepatu Formal Kickers Leatherette boots 2309 Original - Black', 1749000, 1756000, NULL, 'ecl-sp-223396146', '2017-11-10 20:57:47'),
(390, 1, 'ECL-2-126', '223395577', 'Sepatu Formal Kickers Leatherette boots 2308 Original - Coffee', 2289000, 2296000, NULL, 'ecl-sp-223395577', '2017-11-10 20:57:47'),
(391, 1, 'ECL-2-127', '223395076', 'Sepatu Formal Kickers Leatherette boots 2308 Original - Black', 2289000, 2296000, NULL, 'ecl-sp-223395076', '2017-11-10 20:57:48'),
(392, 1, 'ECL-2-128', '223394150', 'Sepatu Kickers Casual Leather 2316 Original - Grey/Blue', 1749000, 1756000, NULL, 'ecl-sp-223394150', '2017-11-10 20:57:49'),
(393, 1, 'ECL-2-129', '223393779', 'Sepatu Kickers Casual Leather 2317 Original - Coffee', 1769000, 1776000, NULL, 'ecl-sp-223393779', '2017-11-10 20:57:50'),
(394, 1, 'ECL-2-130', '223393398', 'Sepatu Kickers Casual Leather 2317 Original - Navy', 1769000, 1776000, NULL, 'ecl-sp-223393398', '2017-11-10 20:57:50'),
(395, 1, 'ECL-2-131', '223392703', 'Sepatu Kickers Casual Leather 2316 Original - Dark Brown Red', 1749000, 1756000, NULL, 'ecl-sp-223392703', '2017-11-10 20:57:51'),
(396, 1, 'ECL-2-132', '223392013', 'Sepatu Kickers Slip On Leather 2322 Original - Tan', 1749000, 1756000, NULL, 'ecl-sp-223392013', '2017-11-10 20:57:52'),
(397, 1, 'ECL-2-133', '223391339', 'Sepatu Kickers Slip On Leather 2322 Original - Coffee', 1699000, 1706000, NULL, 'ecl-sp-223391339', '2017-11-10 20:57:52'),
(398, 1, 'ECL-2-134', '223390524', 'Sepatu Kickers Slip On Casual Leather 2319 Original - Tan', 1699000, 1706000, NULL, 'ecl-sp-223390524', '2017-11-10 20:57:53'),
(399, 1, 'ECL-2-135', '223390060', 'Sepatu Formal Kickers Slip On Casual Leather 2319 Original - Coffee', 1699000, 1706000, NULL, 'ecl-sp-223390060', '2017-11-10 20:57:54'),
(400, 1, 'ECL-2-136', '223224138', 'Sepatu Original Puma Disc Blaze CT Core Sneakers - Black', 1499000, 1506000, NULL, 'ecl-sp-223224138', '2017-11-10 20:57:55'),
(401, 1, 'ECL-2-137', '223213047', 'Sepatu Original Puma Blaze Multi - Black/White', 1199000, 1206000, NULL, 'ecl-sp-223213047', '2017-11-10 20:57:56'),
(402, 1, 'ECL-2-138', '223214602', 'Sepatu Original Puma Blaze Multi - Grey/White', 1199000, 1206000, NULL, 'ecl-sp-223214602', '2017-11-10 20:57:57'),
(403, 1, 'ECL-2-139', '223216837', 'Sepatu Original Puma Match Select Premium Sneakers - Black/White', 1199000, 1206000, NULL, 'ecl-sp-223216837', '2017-11-10 20:57:57'),
(404, 1, 'ECL-2-140', '223207581', 'Sepatu Original New Balance Vazee Rush Performance - Deep Blue ', 1299000, 1306000, NULL, 'ecl-sp-223207581', '2017-11-10 20:57:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `master_category`
--
ALTER TABLE `master_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `master_product`
--
ALTER TABLE `master_product`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `master_category`
--
ALTER TABLE `master_category`
  MODIFY `id` int(15) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `master_product`
--
ALTER TABLE `master_product`
  MODIFY `id` mediumint(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=405;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
